package com.example.todo.data

import androidx.compose.runtime.mutableStateListOf
import com.example.todo.model.Task

object TaskSingleton {
    private val tasks = mutableStateListOf<Task>()
    fun updateTasks(tasks: ArrayList<Task>) {
        this.tasks.clear()
        this.tasks.addAll(tasks)
    }
    fun getTasks(): List<Task> {
        return this.tasks
    }
}