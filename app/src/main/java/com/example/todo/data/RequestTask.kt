package com.example.todo.data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.android.volley.Request
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.todo.model.Task
import org.json.JSONArray
import org.json.JSONObject

class RequestTask(context: Context) {
    private val queue = Volley.newRequestQueue(context)

    companion object {
        private const val constanteEnderecoAPI = "http://10.0.2.2:8100"
    }

    fun startRequestingTasks()
    {
        val handler = Handler(Looper.getMainLooper())
        handler.post(object : Runnable {
            override fun run() {
                processRequest()
                handler.postDelayed(this, 15000)
            }
        })
    }
    private fun processRequest()
    {
        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET,
            "${constanteEnderecoAPI}/tasks",
            null,
            { response ->
                val tasks = this.JSONArrayToTasks(response)
                TaskSingleton.updateTasks(tasks)
            },
            { volleyError ->
                Log.e("RequestTaskError", "Connection error. ${volleyError.toString()}")
            }
        )
        this.queue.add(jsonArrayRequest)
    }
    fun JSONArrayToTasks(jsonArray: JSONArray): ArrayList<Task>
    {
        val tasks = ArrayList<Task>()
        for (i in 0..(jsonArray.length()-1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val id = jsonObj.getInt("id")
            val description = jsonObj.getString("description")
            val isDone = jsonObj.getInt("isDone") == 1
            val isUrgent = jsonObj.getInt("isUrgent") == 1
            tasks.add(
                Task(
                    description,
                    isUrgent,
                    isDone,
                    id
                )
            )
        }

        return tasks
    }
    fun adicionarTarefaAPI(task:Task){
        var body = JSONObject();
        body.put("description", task.description)
        body.put("isDone", task.isDone)
        body.put("isUrgent", task.isUrgent)

        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.POST,
            "${constanteEnderecoAPI}/tasks/new",
            body,
            { response -> processRequest()},
            { volleyError ->}
        )

        this.queue.add(jsonArrayRequest);
    }

    fun atualizarStatusTarefa(id: Int, isDone: Boolean){
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.PUT,
            "${constanteEnderecoAPI}/tasks/done/${isDone}/${id}",
            JSONObject(),
            { response -> processRequest()},
            { volleyError ->}
        )

        this.queue.add(jsonArrayRequest);
    }

    fun deletarTarefa(id: Int){
        val jsonArrayRequest = JsonObjectRequest(
            Request.Method.DELETE,
            "${constanteEnderecoAPI}/tasks/del/${id}",
            JSONObject(),
            { response -> processRequest()},
            { volleyError ->}
        )

        this.queue.add(jsonArrayRequest);
    }
}