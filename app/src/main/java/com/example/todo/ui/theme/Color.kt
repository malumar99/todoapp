package com.example.todo.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Rosa = Color(0xFFFF007F)
val Verde = Color(0xFFFF39F78F)
val Vermelho = Color(0xFFFFED374B)