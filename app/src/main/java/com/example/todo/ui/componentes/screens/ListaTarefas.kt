package com.example.todo.ui.componentes.screens

import android.content.Context
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.todo.data.RequestTask
import com.example.todo.data.TaskSingleton
import com.example.todo.model.Task
import com.example.todo.ui.theme.Purple500
import com.example.todo.ui.theme.ToDoTheme
import com.example.todo.ui.theme.Verde
import com.example.todo.ui.theme.Vermelho

@Composable
fun ListaTarefas(requestTask: RequestTask) {

    var checkedState = remember { mutableStateOf(false) }
    var text = remember { mutableStateOf(TextFieldValue("")) }
    Surface(
        modifier =
        Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.background)
    ) {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = "ToDoApp",
                            color =
                            if(MaterialTheme.colors.isLight)
                                Color.White
                            else
                                Color.White,
                            fontSize = 20.sp
                        )
                    },
                    backgroundColor = MaterialTheme.colors.primary,
                    elevation = 10.dp
                )
            },
            bottomBar = {
                Column(){
                    Row(){
                        Text("Urgente ?", modifier = Modifier.align(Alignment.CenterVertically).padding(start = 15.dp),
                            fontSize = 22.sp, fontWeight = FontWeight.Bold)
                        Switch(checked = checkedState.value, onCheckedChange = { bool -> checkedState.value = bool})
                    }
                    Row(){
                        TextField(value = text.value, modifier = Modifier.fillMaxWidth().padding(horizontal = 15.dp, vertical = 5.dp),
                            onValueChange = { textFieldValue: TextFieldValue -> text.value =  textFieldValue},
                            placeholder = { Text("Descreva a tarefa...")})
                    }
                    Row(){
                        Button(modifier = Modifier.fillMaxWidth().padding(horizontal = 15.dp, vertical = 5.dp).height(50.dp),
                        onClick = {
                            val novaTarefa:Task = Task(text.value.text,checkedState.value,false,null);
                            requestTask.adicionarTarefaAPI(novaTarefa)
                            text.value = TextFieldValue("");
                        }){
                            Text("Salvar Tarefa", fontSize = 20.sp)
                        }
                    }
                }
            }
        ) { innerPadding ->
            Box(
                modifier = Modifier.padding(innerPadding)
            ) {
                val movieList = remember {
                    TaskSingleton.getTasks()
                }
                Column(){
                    LazyColumn {
                        items(movieList) { task ->
                            TaskItemView(task = task, requestTask)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun TaskItemView(task: Task,requestTask: RequestTask) {
    var checkedState by remember {
        mutableStateOf(false)
    }
    checkedState = task.isDone;
    var mostrarTudo = remember { mutableStateOf(false) }
    var mostrarAlertaExclusao = remember { mutableStateOf(false) }
    var corEtiqueta = Verde;
    if(task.isUrgent == true)
        corEtiqueta = Vermelho;

    Card(modifier =
    Modifier
        .fillMaxWidth()
        .padding(vertical = 2.5.dp, horizontal = 5.dp)
        .pointerInput(Unit) {
            detectTapGestures(
                onLongPress = {
                    if (mostrarAlertaExclusao.value == true)
                        mostrarAlertaExclusao.value = false
                    else
                        mostrarAlertaExclusao.value = true;
                },
                onTap = {
                    if (mostrarTudo.value == true)
                        mostrarTudo.value = false
                    else
                        mostrarTudo.value = true;
                }
            )
        },

        backgroundColor = Color.White
    ) {
        Row (Modifier.height(intrinsicSize = IntrinsicSize.Min)){
            Box(modifier = Modifier
                .width(20.dp)
                .background(corEtiqueta)
                .fillMaxHeight())
            Checkbox(checked = checkedState,
                onCheckedChange = {
                    if(task.id != null){
                        checkedState = it
                        task.isDone = it;
                        requestTask.atualizarStatusTarefa(id = task.id, isDone = task.isDone);
                    }
                }, Modifier.width(50.dp))
            Text(
                task.description,
                fontSize = 23.sp,
                color = MaterialTheme.colors.primary,
                fontWeight = FontWeight.Bold,
                maxLines = if(mostrarTudo.value == true) 9999 else 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier.align(Alignment.CenterVertically)
            )
        }
        if (mostrarAlertaExclusao.value) {

            AlertDialog(
                onDismissRequest = {

                },
                title = {
                    Text(text = "Deseja realmente excluir esta tarefa ?")
                },
                confirmButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Vermelho,
                            contentColor = Color.White),
                        onClick = {
                            if(task.id != null){
                                requestTask.deletarTarefa(task.id);
                            }
                            mostrarAlertaExclusao.value = false;
                        }) {
                        Text("Sim")
                    }
                },
                dismissButton = {
                    Button(
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Verde,
                            contentColor = Color.White),
                        onClick = {
                            mostrarAlertaExclusao.value = false;
                        }) {
                        Text("Não")
                    }
                }
            )
        }
    }
}

@Composable
fun AlertDialogSample() {
    MaterialTheme {
        Column {
            val openDialog = remember { mutableStateOf(false)  }

            Button(onClick = {
                openDialog.value = true
            }) {
                Text("Click me")
            }


        }

    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val context = LocalContext.current;
    var requestTask: RequestTask = RequestTask(context);
    ToDoTheme {
        TaskItemView(Task("Terminar TFC1",true,false,1),requestTask);
    }
}